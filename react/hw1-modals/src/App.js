import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';


export default class App extends Component {
  state = {
    isModalOpen: false,
    isSecondModalOpen: false
  }

  openModal = () => {
    this.setState({isModalOpen: true})
  }

  openSecondModal = () => {
    this.setState({isSecondModalOpen: true})
  }

  closeModal = () => {
    

    this.setState({isModalOpen: false})
  }

  closeSecondModal = () => {

    this.setState({isSecondModalOpen: false})
  }

  render() {
    const {isModalOpen, isSecondModalOpen} = this.state;
    return (
      <div>
        <Button
         text="Open first modal"
         backgroundColor="gold"
         onClick={this.openModal}
         />
         <Button
         text="Open second modal"
         backgroundColor="silver"
         onClick={this.openSecondModal}
         theme="modal-btn-silver"
         />
        {isModalOpen && <Modal 
        header="Do you want to delete this file?"
        closeButton={this.closeModal}
        text="Once you delete this file, it won`t be possible to undo this action."
        question="Are you sure you want to delete it?"
        actions={
          <React.Fragment>
          <Button 
          text="OK"
          backgroundColor="rgba(199, 61, 61, 0.5)"
          theme="action-btn"
          />
          <Button 
          text="Cancel"
          backgroundColor="rgba(199, 61, 61, 0.7)"
          theme="action-btn"
          onClick={this.closeModal}
          />
          </React.Fragment>
        }
        />}
        {isSecondModalOpen && <Modal 
        header="Do you want to leave this site?"
        closeButton={this.closeSecondModal}
        text="After leaving this site you`ll be logged out."
        question="Are you sure you want to log out and leave this site?"
        modalTheme="second-modal"
        actions={
          <React.Fragment>
          <Button 
          text="Stay"
          backgroundColor="rgba(67, 186, 233, 0.7)"
          theme="action-btn"
          />
          <Button 
          text="Leave"
          backgroundColor="rgba(67, 186, 233, 0.7)"
          theme="action-btn"
          onClick={this.closeSecondModal}
          />
          </React.Fragment>
        }
        />}
      </div>
    )
  }
}



