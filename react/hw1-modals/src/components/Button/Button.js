import React, { Component } from 'react';
import "./Button.css";


export default class Button extends Component {

    getWrapperClass(theme) {
        switch(theme) {
            case "action-btn": 
                return "action-btn-wrapper";
            default:
                return "btn-wrapper";
        }
    }

    getButtonClass(theme) {
        switch(theme) {
            case "action-btn":
                return "action-btn";
            case "modal-btn-silver":
                return "btn-silver";
            default: 
                return "modal__btn";
        }
    }

    render() {
        const {backgroundColor, text, onClick, theme} = this.props;
        return (
            <div className={this.getWrapperClass(theme)}>
                <button 
                className={this.getButtonClass(theme)}
                style={{backgroundColor: backgroundColor}}
                onClick={onClick}
                >
                    {text}
                    </button>
            </div>
        )
    }
}
