import React, { Component } from 'react';
import "./Modal.css";

export default class Modal extends Component {
    render() {
        const {header, text, question, actions, closeButton, modalTheme} = this.props;
        return (
            <div 
                className="modal-wrapper"
                id="modal"
                onClick={(e) => {
                    if (e.target.id !== "modal") {
                        return;
                    }
                    closeButton();
                }}
            >
                <div className={modalTheme === "second-modal" ? "modal second-modal-container" : "modal"}>
                    <div className={modalTheme === "second-modal" ? "modal__header second-modal-header" : "modal__header"}>
                        <p className="modal__header__title">{header}</p>
                        <span 
                        className="modal__header__close-btn"
                        onClick={closeButton}
                        >
                            X
                            </span>
                    </div>
                    <div className="modal__content">
                        <p className="modal__content__text">{text}</p>
                        <p className="modal__content__question">{question}</p>
                    </div>
                    <div className="modal__footer">{actions}</div>
                </div>
            </div>
        )
    }
}
