import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Button.css";

export default class Button extends Component {
  getWrapperClass(theme) {
    switch (theme) {
      case "action-btn":
        return "action-btn-wrapper";
      default:
        return "btn-wrapper";
    }
  }

  getButtonClass(theme) {
    switch (theme) {
      case "action-btn":
        return "action-btn";
      case "card-btn":
        return "card-btn-item";
      default:
        return "modal__btn";
    }
  }

  render() {
    const { backgroundColor, text, onClick, theme } = this.props;
    return (
      <div className={this.getWrapperClass(theme)}>
        <button
          className={this.getButtonClass(theme)}
          style={{ backgroundColor: backgroundColor }}
          onClick={onClick}
        >
          {text}
        </button>
      </div>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  backgroundColor: PropTypes.string,
  theme: PropTypes.string,
};
