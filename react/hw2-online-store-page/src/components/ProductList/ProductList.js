import React, { Component } from "react";
import PropTypes from "prop-types";
import Product from "../Product/Product";
import "./ProductList.css";

export default class ProductList extends Component {
  render() {
    const { goods, showModal, isFavorite, toogleFavorite } = this.props;
    const productItems = goods.map((product) => (
      <Product
        key={product.id}
        product={product}
        showModal={showModal}
        isFavorite={isFavorite(product.id)}
        toogleFavorite={() => toogleFavorite(product.id)}
      />
    ));
    return <div className="card">{productItems}</div>;
  }
}

ProductList.propTypes = {
  goods: PropTypes.array.isRequired,
  showModal: PropTypes.func.isRequired,
  isFavorite: PropTypes.func.isRequired,
  toogleFavorite: PropTypes.func.isRequired,
};
