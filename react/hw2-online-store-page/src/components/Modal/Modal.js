import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Modal.css";

export default class Modal extends Component {
  render() {
    const { header, text, actions, closeButton } = this.props;
    return (
      <div
        className="modal-wrapper"
        id="modal"
        onClick={(e) => {
          if (e.target.id !== "modal") {
            return;
          }
          closeButton();
        }}
      >
        <div className="modal">
          <div className="modal__header">
            <p className="modal__header__title">{header}</p>
            <span className="modal__header__close-btn" onClick={closeButton}>
              X
            </span>
          </div>
          <div className="modal__content">
            <p className="modal__content__text">{text}</p>
          </div>
          <div className="modal__footer">{actions}</div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired,
  closeButton: PropTypes.func.isRequired,
};
