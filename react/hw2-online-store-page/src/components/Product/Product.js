import React, { Component } from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import "./Product.css";
import { ReactComponent as FavoriteLogo } from "../../icons/star.svg";
import { ReactComponent as FilledIcon } from "../../icons/star-yellow.svg";

export default class Product extends Component {
  render() {
    const { product, showModal, isFavorite, toogleFavorite } = this.props;
    const { title, price, imagePath, setNumber, color, id } = product;
    return (
      <div className="card-item">
        <div className="favorite">
          {isFavorite ? (
            <FilledIcon onClick={toogleFavorite} />
          ) : (
            <FavoriteLogo onClick={toogleFavorite} />
          )}
        </div>
        <img className="card-img" src={imagePath} alt={title} />
        <div className="card-content">
          <p className="card-content-title">{title}</p>
          <p className="card-content-setNumber">Set number: {setNumber}</p>
          <p className="card-content-color">Color: {color}</p>
          <p className="card-content-price">₴{price}</p>
          <Button
            text="Add to card"
            backgroundColor="gold"
            theme="card-btn"
            onClick={() => showModal(id)}
          />
        </div>
      </div>
    );
  }
}

Product.propTypes = {
  product: PropTypes.object.isRequired,
  showModal: PropTypes.func.isRequired,
  toogleFavorite: PropTypes.func.isRequired,
  isFavorite: PropTypes.bool,
};

Product.defaultProps = {
  isFavorite: false,
};
