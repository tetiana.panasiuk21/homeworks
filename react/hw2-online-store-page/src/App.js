import "./App.css";
import React, { Component } from "react";
import axios from "axios";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import ProductList from "./components/ProductList/ProductList";

export default class App extends Component {
  state = {
    isModalOpen: false,
    goods: [],
    favoriteProducts:
      JSON.parse(window.localStorage.getItem("favorites")) || [],
    productModalId: 0,
  };

  componentDidMount() {
    axios("/goods.json").then((res) => this.setState({ goods: res.data }));
  }

  openModal = (id) => {
    this.setState({ isModalOpen: true, productModalId: id });
  };

  closeModal = () => {
    this.setState({ isModalOpen: false });
  };

  confirmAdding = () => {
    const products = JSON.parse(window.localStorage.getItem("cart")) || [];
    if (!products.includes(this.state.productModalId)) {
      products.push(this.state.productModalId);
      window.localStorage.setItem("cart", JSON.stringify(products));
    }
    this.closeModal();
  };

  isFavorite = (id) => this.state.favoriteProducts.includes(id);

  toogleFavorite = (id) => {
    let favorites = [];
    if (this.isFavorite(id)) {
      favorites = this.state.favoriteProducts.filter((i) => i !== id);
    } else {
      favorites = [...this.state.favoriteProducts, id];
    }
    window.localStorage.setItem("favorites", JSON.stringify(favorites));
    this.setState({ favoriteProducts: favorites });
  };

  render() {
    const { isModalOpen, goods } = this.state;
    return (
      <div>
        {isModalOpen && (
          <Modal
            header={
              "Are you sure you want to add this product to shopping cart?"
            }
            closeButton={this.closeModal}
            text="Please, choose the next action!"
            actions={
              <React.Fragment>
                <Button
                  text="Confirm"
                  backgroundColor="rgba(199, 61, 61, 0.5)"
                  theme="action-btn"
                  onClick={this.confirmAdding}
                />
                <Button
                  text="Dismiss"
                  backgroundColor="rgba(199, 61, 61, 0.7)"
                  theme="action-btn"
                  onClick={this.closeModal}
                />
              </React.Fragment>
            }
          />
        )}
        <ProductList
          goods={goods}
          showModal={this.openModal}
          isFavorite={this.isFavorite}
          toogleFavorite={this.toogleFavorite}
        />
      </div>
    );
  }
}
