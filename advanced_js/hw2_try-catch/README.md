### Відповідь на теоретичне запитання:

Коли ми думаємо, що якийсь шматок коду може мати помилку і ми маємо чіткий алгоритм дій, як обіграти цю дію - видавання помилки, тоді ми використовуємо try...catch. Тобто ми говоримо, що ось тут небезпека, а ось так її можна вирішити.

Які є приклади:

- Якщо користувач ввів неправильні дані.
- Коли в response прийшли неочікувані дані.
