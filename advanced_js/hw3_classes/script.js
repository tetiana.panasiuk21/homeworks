class Employee {
  #_name;
  #_age;
  #_salary;

  constructor(name, age, salary) {
    this.#_name = name;
    this.#_age = age;
    this.#_salary = salary;
  }
  get name() {
    return this.#_name;
  }
  set name(newName) {
    this.#_name = newName;
  }
  get age() {
    return this.#_age;
  }
  set age(newAge) {
    this.#_age = newAge;
  }
  get salary() {
    return this.#_salary;
  }
  set salary(newSalary) {
    this.#_salary = newSalary;
  }
}

class Programmer extends Employee {
  #_lang;
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.#_lang = lang;
  }
  get salary() {
    return super.salary * 3;
  }
}

const employee1 = new Programmer("Tom", 23, 2000, "Java");
const employee2 = new Programmer("Tim", 29, 3000, "PHP");
const employee3 = new Programmer("Tania", 27, 300, "JavaScript");

console.log(employee1);
console.log(employee2);
console.log(employee3);
