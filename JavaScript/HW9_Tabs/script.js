const navTabs = document.querySelectorAll(".tabs-title");
navTabs.forEach((item) => {
  item.addEventListener("click", (event) => {
    const navElem = event.target;
    const liClassName = navElem.getAttribute("data-tab");
    const contentBlocks = document.querySelectorAll(".list-item");

    contentBlocks.forEach((contentBlock) => {
      if (contentBlock.classList.contains(liClassName)) {
        contentBlock.style.display = "block";
      } else {
        contentBlock.style.display = "none";
      }
    });

    navTabs.forEach((tab) => {
      tab.classList.remove("active");
    });
    item.classList.add("active");
  });
});
