createNewUser = function () {
  return {
    firstName: prompt("Enter your first name", "Tania"),
    lastName: prompt("Enter your last name", "Panasiuk"),
    age: 0,
    birthdayDate: null,
    getAge() {
      if (this.age) {
        return this.age;
      }
      let userBirth = prompt(
        "Enter your birthday date in such format - dd.mm.yyyy",
        "21.08.1993"
      );
      let birthNum = userBirth.split(".");
      let now = new Date();
      this.birthdayDate = new Date();
      this.birthdayDate.setFullYear(birthNum[2]);
      this.birthdayDate.setMonth(birthNum[1] - 1);
      this.birthdayDate.setDate(birthNum[0]);
      let age = now.getFullYear() - this.birthdayDate.getFullYear();
      if (now.getMonth() < this.birthdayDate.getMonth()) {
        --age;
      } else if (
        now.getMonth() == this.birthdayDate.getMonth() &&
        now.getDate() < this.birthdayDate.getDate()
      ) {
        --age;
      }
      this.age = age;
      return age;
    },
    getPassword() {
      return `${this.firstName
        .charAt(0)
        .toUpperCase()}${this.lastName.toLowerCase()}${this.birthdayDate.getFullYear()}`;
    },
    getLogin() {
      return `${this.firstName.charAt(0)}${this.lastName}`.toLowerCase();
    },
  };
};
let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());

// Відповідь на теоретичне запитання:
// Екранування - це спосіб виведення спецсимволів у рядку.
// Тобто, щоб спецсимвол зчитувався JavaScript, як рядковий елемент, потрібно
// задіяти для цього додаткові символи, які ще називають символами екранування.
// Одним із символів екранування є зворотний слеш (прийнаймні найпоширеніший).
