let userNumber = +prompt("Enter number");
while (userNumber !== parseInt(userNumber)) {
  userNumber = +prompt("Enter integer number");
}
if (userNumber >= 5) {
  for (let i = 1; i <= userNumber; i++) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
} else {
  console.log("Sorry, no numbers");
}

// Відповідь на теоретичне запитання:

// Цикли дають змогу виконати один і той же шматок коду багато разів. Тобто це певний блок команд, які повторюються до того моменту,
// поки певна умова не виконається. Є такі види циклів у JS: for, while, do while (ще бачила for in та for of).
