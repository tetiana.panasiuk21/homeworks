const filterBy = (array, dataType) =>
  array.filter((element) => typeof element !== dataType);

console.log(filterBy(["hello", "world", 23, "23", null], "number"));

// Відповідь на теоретичне запитання:
// Метод forEach відповідає за перебір елементів масиву. Тобто по черзі береться елемент і з ним
// проводяться певні маніпуляції згідно з callback-функцією (тобто дії, описані в callback-функції).
