const input = document.querySelector("input");

const focusHandler = (event) => {
  event.target.style.border = "2px solid green";
};

const blurHandler = (notFocus) => {
  const targetInput = notFocus.target;
  const inputValue = targetInput.value;
  let spanCost = document.querySelector("form span.cost");
  if (!spanCost) {
    const spanElement = document.createElement("span");
    targetInput.parentElement.prepend(spanElement);
    spanElement.classList.add("span-class");
    spanElement.innerText = `Текущая цена: `;

    spanCost = document.createElement("span");
    spanCost.classList.add("cost");
    spanElement.append(spanCost);

    const spanBtn = document.createElement("span");
    spanBtn.innerText = "X";
    spanBtn.classList.add("delete");
    spanElement.append(spanBtn);
    spanElement.append(document.createElement("br"));
    spanBtn.addEventListener("click", (event) => {
      event.target.parentElement.remove();
      targetInput.value = "";
    });
  }

  const errorSpan = document.querySelector("span.red");
  if (inputValue < 0) {
    if (errorSpan.classList.contains("hidden")) {
      errorSpan.classList.remove("hidden");
    }
    const infoSpan = document.querySelector(".span-class");
    if (infoSpan) {
      infoSpan.remove();
    }
    targetInput.style.border = "2px solid red";
    return;
  } else if (!errorSpan.classList.contains("hidden")) {
    errorSpan.classList.add("hidden");
  }
  spanCost.innerText = inputValue;
  targetInput.style.border = "";
};

input.addEventListener("focus", focusHandler);
input.addEventListener("blur", blurHandler);
