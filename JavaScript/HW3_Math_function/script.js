function getValidUserNamuber(message, againMessage) {
  let userNum1 = +prompt(message);
  while (Number.isNaN(userNum1)) {
    userNum1 = +prompt(againMessage);
  }
  return userNum1;
}

let userNum1 = getValidUserNamuber(
  "Enter the first number",
  "Enter the correct first number"
);
let userNum2 = getValidUserNamuber(
  "Enter the second number",
  "Enter the correct second number"
);

let operation = prompt("Enter a mathematical operation");
while (
  operation !== "+" &&
  operation !== "-" &&
  operation !== "*" &&
  operation !== "/"
) {
  operation = prompt("Enter mathematical operation correctly");
}

function getData(userNum1, userNum2, operation) {
  switch (operation) {
    case "+":
      return userNum1 + userNum2;
    case "-":
      return userNum1 - userNum2;
    case "*":
      return userNum1 * userNum2;
    case "/":
      return userNum1 / userNum2;
  }
}

console.log(getData(userNum1, userNum2, operation));

// Відповіді на теоретичні запитання:

// 1. Функції - це так звані "будівельні блоки" програми - фрагмент коду, який ми прописуємо один раз.
// Тому це дозволяє нам не повторювати один і той же код, а просто викликати
// функцію, звернувшись до неї за іменем, у будь-які частині програми стільки разів, скільки нам потрібно.

// 2. Аргументи - це складові функції, які ми прописуємо в круглих дужках після імені функції для того, щоб далі з ними працювати.
// Тобто, якщо функції потрібна певна зовнішня інформація, то ми записуємо (передаємо) її за допомогою аргументів.
// Тому можна сказати, що аргументи - це певна залежність функції від певних змінних, тобто від інформації ззовні.
// І якщо їх (аргументи) передати неправильні чи взагалі не передати,
// то функція відпрацює неправильно.
