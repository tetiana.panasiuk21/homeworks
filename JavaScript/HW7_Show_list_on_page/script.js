function createListOfItems(someArray, parentElement = document.body) {
  const ul = document.createElement("ul");
  ul.innerHTML = someArray.map((item) => `<li>${item}</li>`).join("");
  parentElement.append(ul);
}
createListOfItems(["hello", "world", "Kyiv", "Kharkiv", "Odesa", "Lviv"]);

// function createListOfItemsUsingNodes(someArray, parentElement = document.body) {
//   const ul = document.createElement("ul");
//   someArray.forEach((item) => {
//     const li = document.createElement("li");
//     li.innerText = item;
//     ul.append(li);
//   });
//   parentElement.append(ul);
// }
// createListOfItemsUsingNodes([
//   "hello",
//   "world",
//   "Kyiv",
//   "Kharkiv",
//   "Odessa",
//   "Lviv",
// ]);

// Відповідь на теоретичне запитання:
// Document Object Model (DOM) - це об'єктна модель документа, інструмент, за допомогою якого
// можна керувати вмістом веб-сторінки. В межах цієї моделі вмістом документа є об'єкти (тобто html-теги
// є об'єктами), між якими є певні зв'язки (тут можна визначити батьківські, дочірні елементи). Відповідно, усі ці об'єкти
// доступні завдяки JS, і їх можна використовувати для внесення змін веб-сторінки.
