createNewUser = function () {
  let name = prompt("Enter your first name", "");
  let surname = prompt("Enter your last name", "");
  //   Далі йде об'єкт newUser. Я його створила, а потім подивилася, що можна ще спростити і спростила до такого вигляду. Ну і вже повторно його не вказувала.
  return {
    firstName: name,
    lastName: surname,
    getLogin() {
      return `${this.firstName.charAt(0)}${this.lastName}`.toLowerCase();
    },
  };
};
let user = createNewUser();
console.log(user.getLogin());

// Відповідь на теоретичне запитання:
// Метод об'єкта - це по суті властивість об'єкта, значенням якої є функція. Або ще можна сказати, що
// метод - це функція, вбудована в об'єкт.
