function User(name, surname) {
  // local vars visible only from getters and setters
  let firstName = name;
  let lastName = surname;

  // define name and getter and setter
  Object.defineProperty(this, "firstName", {
    get: function () {
      return firstName;
    },
    set: function (value) {
      firstName = value;
    },
  });

  // define lastName and getter and setter
  Object.defineProperty(this, "lastName", {
    get: function () {
      return lastName;
    },
    set: function (newValue) {
      lastName = newValue;
    },
  });

  this.getLogin = function () {
    return `${this.firstName.charAt(0)}${this.lastName}`.toLowerCase();
  };
}

const createNewUser = function () {
  let name = prompt("Enter your first name", "");
  let surname = prompt("Enter your last name", "");
  return new User(name, surname);
};
let user = createNewUser();
console.log(user.getLogin());
